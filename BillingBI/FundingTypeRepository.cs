﻿using System;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BillingCommon.DTO;
using Core.DAL;
using log4net;


namespace BI
{
    public interface IFundingTypeRepository
    {
        IQueryable<FundingTypeDto> GetAllSupportedFundingTypes();

        IQueryable<FundingTypeDto> GetAllSupportedFundingTypeById(int iFundingTypeId);

        IQueryable<FundingTypeSupportedCurrenciesDto> GetFundingTypeSupportedCurrencies();

        IQueryable<FundingTypeSupportedCurrenciesDto> GetFundingTypeSupportedCurrencyById(int iFundingTypeId);

        IQueryable<AvailForSetTestDto> SetFundingTypeDeposits(int iFundingTypeId, int iDepositId);

        IQueryable<AvailForSetTestDto> GetAvailDepositIdAndFundingTypeIdForTest();
    }
    public class FundingTypeRepository : IFundingTypeRepository
    {
        private static readonly ILog _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly BillingContext _context;
        private readonly DbSet<FundingType> _dbSetFundingTypes;

        public FundingTypeRepository()
        {
            _logger.Debug($"Called ctor::FundingTypeRepository()");
            _context = new BillingContext();
            _dbSetFundingTypes = _context.Set<FundingType>();
            Mapper.Initialize(cfg => cfg.CreateMap<FundingType, FundingTypeDto>());

        }
        /// <summary>
        /// Get all data from Tbl FundingType
        /// </summary>
        /// <returns>IQueryable&lt;FundingTypeDTO&gt;</returns> - AutoMapper DTO object describes all supported funding types
        public IQueryable<FundingTypeDto> GetAllSupportedFundingTypes()
        {
            return _dbSetFundingTypes.ProjectTo<FundingTypeDto>().AsQueryable();
        }

        /// <summary>
        /// Get concrete FundingType from table FundingType by FundingTypeId
        /// </summary>
        /// <returns>IQueryable&lt;FundingTypeDTO&gt;</returns> - AutoMapper DTO object describes all supported funding types
        public IQueryable<FundingTypeDto> GetAllSupportedFundingTypeById(int iFundingTypeId)
        {
            return _dbSetFundingTypes.Where(z => z.Id == iFundingTypeId).AsQueryable()
                .ProjectTo<FundingTypeDto>().AsQueryable();
        }

        /// <summary>
        /// Get data from table FundingTypeCurrency &amp; FundingType based on FundingTypeID
        /// </summary>
        /// <returns>IQueryable&lt;FundingTypeSupportedCurrenciesDTO&gt;</returns> - AutoMapper DTO object describes all supported funding types for all supported currencies
        public IQueryable<FundingTypeSupportedCurrenciesDto> GetFundingTypeSupportedCurrencies()
        {
            // OutPut : FundingType / Min Deposit / Max Deposit / All Supported Currencies (Support Currency ID Range) in case FundingType have no supported Currencies need return -1.
            return _context.Database
                .SqlQuery<FundingTypeSupportedCurrenciesDto>("exec [dbo].[GetFundingTypeSupportedCurrencies]")
                .AsQueryable();
        }

        public IQueryable<FundingTypeSupportedCurrenciesDto> GetFundingTypeSupportedCurrencyById(int iFundingTypeId)
        {
            SqlParameter iFundingTypeIdParam = new SqlParameter("@FundingTypeID", iFundingTypeId);

            return _context.Database
                .SqlQuery<FundingTypeSupportedCurrenciesDto>("exec [dbo].[GetFundingTypeSupportedCurrencies] @FundingTypeID", iFundingTypeIdParam)
                .AsQueryable();
        }

        /// <summary>
        /// Set Payment Status of Deposit (Deposit table) - PaymentStatusID Approved(PaymentStatus table) based on the income params:
        /// FundingTypeID ​and DepositID with some prerequisites before update operation.
        /// </summary>
        /// <param name="iFundingTypeId"> is FundingTypeID</param>
        /// <param name="iDepositId">is DepositID</param>
        public IQueryable<AvailForSetTestDto> SetFundingTypeDeposits(int iFundingTypeId, int iDepositId)
        {
            SqlParameter iFundingTypeIdParam = new SqlParameter("@FundingTypeID", iFundingTypeId);
            SqlParameter iDepositIdParam = new SqlParameter("@DepositID", iDepositId);

            return _context.Database.SqlQuery<AvailForSetTestDto>("exec [dbo].[SetFundingTypeDeposits] @FundingTypeID, @DepositID", iFundingTypeIdParam, iDepositIdParam)
                .AsQueryable();
        }

        public IQueryable<AvailForSetTestDto> GetAvailDepositIdAndFundingTypeIdForTest()
        {
            return _context.Database
                .SqlQuery<AvailForSetTestDto>("exec [dbo].[GetAvailDepositIdAndFundingTypeIdForTest]")
                .AsQueryable();
        }
    }
}
