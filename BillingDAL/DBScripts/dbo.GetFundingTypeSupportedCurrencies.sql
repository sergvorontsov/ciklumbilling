﻿USE [Billing]
GO

/****** Object:  StoredProcedure [dbo].[GetFundingTypeSupportedCurrencies]    Script Date: 6/13/2017 6:58:49 PM ******/
DROP PROCEDURE [dbo].[GetFundingTypeSupportedCurrencies]
GO

/****** Object:  StoredProcedure [dbo].[GetFundingTypeSupportedCurrencies]    Script Date: 6/13/2017 6:58:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetFundingTypeSupportedCurrencies]
	@FundingTypeID int = NULL
AS

	Select 
		CASE WHEN ft.FundingTypeName IS NULL THEN '-1' ELSE ft.FundingTypeName END as FundingType
		, CASE WHEN ft.MinDeposit IS NULL THEN -1 ELSE ft.MinDeposit END as MinDeposit
		, CASE WHEN ft.MaxDeposit IS NULL THEN -1 ELSE ft.MaxDeposit END as MaxDeposit
		, c.Name as Currency
	from dbo.Currency c WITH (NOLOCK)
		left join dbo.FundingTypeCurrency ftc on c.Id = ftc.CurrencyID
		left join dbo.FundingType ft on ft.Id = ftc.FundingTypeID
	WHERE @FundingTypeID IS NULL OR ftc.FundingTypeID = @FundingTypeID;

RETURN 0

GO


