﻿USE [Billing]
GO

/****** Object:  StoredProcedure [dbo].[SetFundingTypeDeposits]    Script Date: 6/13/2017 6:59:32 PM ******/
DROP PROCEDURE [dbo].[SetFundingTypeDeposits]
GO

/****** Object:  StoredProcedure [dbo].[SetFundingTypeDeposits]    Script Date: 6/13/2017 6:59:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[SetFundingTypeDeposits]
	@FundingTypeID int,
	@DepositID int
AS
Declare @CurrentQuote int;

set @CurrentQuote = (Select 
						SUM(d.PaymentAmount * d.ExchangeRate) CurrentQuote
					from Deposit d with (nolock)
						left join PaymentStatus ps with (nolock) on d.PaymentStatusID = ps.Id
					where 
						d.TimeStamp between convert(datetime, '01/01/2017', 103) and convert(datetime, '01/03/2017', 103)  
						and ps.Status like 'Approved' -- but more  
						and d.Id = @DepositID
						and d.FundingTypeID = @FundingTypeID
					group by d.Id, d.FundingTypeID)

BEGIN TRANSACTION;

BEGIN TRY
	IF (
		@CurrentQuote is not NULL
		AND 
		@CurrentQuote <= (Select 
						ft.MonthlyQuote 
						From FundingType ft with (nolock) 
						where ft.Id = @FundingTypeID)
		AND 
		0 < (SELECT COUNT(ID) 
			FROM Deposit 
			where Id = @DepositID 
				and FundingTypeID = @FundingTypeID 
				and PaymentStatusID = (Select ps.Id from PaymentStatus ps where ps.Status not like 'Approved')
			)
		)
		OR 
		(	@CurrentQuote is null 
			AND 
			0 < (SELECT COUNT(ID) FROM Deposit where Id = @DepositID and FundingTypeID = @FundingTypeID)
		)
	BEGIN
		update Deposit 
			set PaymentStatusID = (Select ps.Id 
									from PaymentStatus ps 
									where ps.Status like 'Approved') 
		where Id = @DepositID and FundingTypeID = @FundingTypeID;
	
		update FundingType 
			set QuoteActive = 0 
		where Id = @FundingTypeID;
		--------------

		Select d.Id DepositID, d.FundingTypeID from Deposit d
		where Id = @DepositID and FundingTypeID = @FundingTypeID;

	END
END TRY
BEGIN CATCH
    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;

RETURN 0

GO


