﻿USE [Billing]
GO

/****** Object:  StoredProcedure [dbo].[GetAvailDepositIdAndFundingTypeIdForTest]    Script Date: 6/13/2017 6:53:53 PM ******/
DROP PROCEDURE [dbo].[GetAvailDepositIdAndFundingTypeIdForTest]
GO

/****** Object:  StoredProcedure [dbo].[GetAvailDepositIdAndFundingTypeIdForTest]    Script Date: 6/13/2017 6:53:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetAvailDepositIdAndFundingTypeIdForTest]
AS
Select d.Id DepositID, d.FundingTypeID
from Deposit d with (nolock)
	left join PaymentStatus ps with (nolock) on d.PaymentStatusID = ps.Id
	left join FundingType ft with (nolock) on d.FundingTypeID = ft.Id
where 
	d.TimeStamp between convert(datetime, '01/01/2017', 103) and convert(datetime, '01/03/2017', 103)  
	and ps.Status not like 'Approved'
group by d.Id, d.FundingTypeID, ft.MonthlyQuote
HAVING SUM(d.PaymentAmount * d.ExchangeRate) <= ft.MonthlyQuote
RETURN 0

GO


