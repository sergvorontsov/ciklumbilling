﻿USE [master]
GO
/****** Object:  Database [Billing]    Script Date: 6/13/2017 7:09:15 PM ******/
CREATE DATABASE [Billing]
GO
ALTER DATABASE [Billing] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Billing].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Billing] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Billing] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Billing] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Billing] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Billing] SET ARITHABORT OFF 
GO
ALTER DATABASE [Billing] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Billing] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Billing] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Billing] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Billing] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Billing] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Billing] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Billing] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Billing] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Billing] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Billing] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [Billing] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Billing] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Billing] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Billing] SET  MULTI_USER 
GO
ALTER DATABASE [Billing] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Billing] SET QUERY_STORE = ON
GO
ALTER DATABASE [Billing] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 10, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO)
GO
USE [Billing]
GO
/****** Object:  User [Billinig_Owner]    Script Date: 6/13/2017 7:09:15 PM ******/
CREATE USER [Billinig_Owner] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  DatabaseRole [SPExec]    Script Date: 6/13/2017 7:09:16 PM ******/
CREATE ROLE [SPExec]
GO
ALTER ROLE [db_owner] ADD MEMBER [Billinig_Owner]
GO
/****** Object:  Schema [BackOffice]    Script Date: 6/13/2017 7:09:18 PM ******/
CREATE SCHEMA [BackOffice]
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 6/13/2017 7:09:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Deposit]    Script Date: 6/13/2017 7:09:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Deposit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PaymentStatusID] [int] NOT NULL,
	[PaymentAmount] [decimal](18, 0) NOT NULL,
	[ExchangeRate] [decimal](18, 2) NOT NULL,
	[FundingTypeID] [int] NOT NULL,
	[CurrencyID] [int] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
 CONSTRAINT [PK_Deposit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[FundingType]    Script Date: 6/13/2017 7:09:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FundingType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FundingTypeName] [varchar](50) NOT NULL,
	[MinDeposit] [decimal](18, 0) NOT NULL,
	[MaxDeposit] [decimal](18, 0) NOT NULL,
	[MonthlyQuote] [decimal](18, 0) NOT NULL,
	[QuoteActive] [decimal](18, 0) NULL,
 CONSTRAINT [PK_FundingType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[FundingTypeCurrency]    Script Date: 6/13/2017 7:09:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FundingTypeCurrency](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrencyID] [int] NOT NULL,
	[FundingTypeID] [int] NOT NULL,
 CONSTRAINT [PK_FundingTypeCurrency] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[PaymentStatus]    Script Date: 6/13/2017 7:09:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](50) NULL,
 CONSTRAINT [PK_PaymentStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET IDENTITY_INSERT [dbo].[Currency] ON 

GO
INSERT [dbo].[Currency] ([Id], [Name]) VALUES (1, N'USD')
GO
INSERT [dbo].[Currency] ([Id], [Name]) VALUES (2, N'EUR')
GO
INSERT [dbo].[Currency] ([Id], [Name]) VALUES (3, N'GBP')
GO
INSERT [dbo].[Currency] ([Id], [Name]) VALUES (4, N'CAD')
GO
INSERT [dbo].[Currency] ([Id], [Name]) VALUES (5, N'RUB')
GO
INSERT [dbo].[Currency] ([Id], [Name]) VALUES (6, N'JPY')
GO
INSERT [dbo].[Currency] ([Id], [Name]) VALUES (7, N'CNY')
GO
INSERT [dbo].[Currency] ([Id], [Name]) VALUES (8, N'AUD')
GO
SET IDENTITY_INSERT [dbo].[Currency] OFF
GO
SET IDENTITY_INSERT [dbo].[Deposit] ON 

GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (192, 2, CAST(173 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 1, 1, CAST(N'2017-01-13T12:44:55.383' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (193, 2, CAST(519 AS Decimal(18, 0)), CAST(1.23 AS Decimal(18, 2)), 1, 2, CAST(N'2017-01-12T12:44:55.387' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (194, 2, CAST(326 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 1, 1, CAST(N'2017-01-11T12:44:55.387' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (195, 2, CAST(727 AS Decimal(18, 0)), CAST(1.23 AS Decimal(18, 2)), 1, 2, CAST(N'2017-01-10T12:44:55.387' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (196, 2, CAST(655 AS Decimal(18, 0)), CAST(1.87 AS Decimal(18, 2)), 1, 3, CAST(N'2017-01-09T12:44:55.387' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (197, 2, CAST(435 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 4, 1, CAST(N'2017-01-08T12:44:55.387' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (198, 2, CAST(337 AS Decimal(18, 0)), CAST(1.56 AS Decimal(18, 2)), 1, 2, CAST(N'2017-01-07T12:44:55.387' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (199, 2, CAST(125 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 1, 1, CAST(N'2017-01-06T12:44:55.387' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (200, 2, CAST(469 AS Decimal(18, 0)), CAST(1.15 AS Decimal(18, 2)), 1, 2, CAST(N'2017-01-05T12:44:55.387' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (201, 2, CAST(134 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 1, 1, CAST(N'2017-01-04T12:44:55.387' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (202, 1, CAST(523 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 1, 1, CAST(N'2017-01-02T12:44:55.390' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (203, 3, CAST(312 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 1, 1, CAST(N'2017-01-01T12:44:55.390' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (204, 1, CAST(723 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 1, 1, CAST(N'2017-01-03T12:44:55.390' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (205, 3, CAST(678 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 1, 1, CAST(N'2017-01-04T12:44:55.390' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (206, 1, CAST(456 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 1, 1, CAST(N'2017-01-05T12:44:55.390' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (207, 3, CAST(314 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 1, 1, CAST(N'2017-01-06T12:44:55.390' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (208, 1, CAST(161 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 1, 1, CAST(N'2017-01-07T12:44:55.390' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (209, 3, CAST(434 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 1, 1, CAST(N'2017-01-08T12:44:55.390' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (210, 2, CAST(1730 AS Decimal(18, 0)), CAST(0.14 AS Decimal(18, 2)), 4, 7, CAST(N'2017-01-13T12:44:55.390' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (211, 2, CAST(519 AS Decimal(18, 0)), CAST(1.23 AS Decimal(18, 2)), 4, 7, CAST(N'2017-01-12T12:44:55.390' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (212, 2, CAST(326 AS Decimal(18, 0)), CAST(1.34 AS Decimal(18, 2)), 4, 7, CAST(N'2017-01-11T12:44:55.390' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (213, 2, CAST(727 AS Decimal(18, 0)), CAST(1.23 AS Decimal(18, 2)), 4, 7, CAST(N'2017-01-10T12:44:55.390' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (214, 2, CAST(6550 AS Decimal(18, 0)), CAST(1.17 AS Decimal(18, 2)), 4, 7, CAST(N'2017-01-09T12:44:55.390' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (215, 2, CAST(435 AS Decimal(18, 0)), CAST(1.16 AS Decimal(18, 2)), 4, 7, CAST(N'2017-01-08T12:44:55.390' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (216, 2, CAST(337 AS Decimal(18, 0)), CAST(1.16 AS Decimal(18, 2)), 4, 7, CAST(N'2017-01-07T12:44:55.393' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (217, 2, CAST(125 AS Decimal(18, 0)), CAST(1.15 AS Decimal(18, 2)), 4, 7, CAST(N'2017-01-06T12:44:55.393' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (218, 2, CAST(469 AS Decimal(18, 0)), CAST(1.15 AS Decimal(18, 2)), 4, 7, CAST(N'2017-01-05T12:44:55.393' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (219, 1, CAST(435 AS Decimal(18, 0)), CAST(1.16 AS Decimal(18, 2)), 4, 7, CAST(N'2017-01-04T12:44:55.393' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (220, 3, CAST(337 AS Decimal(18, 0)), CAST(1.16 AS Decimal(18, 2)), 4, 7, CAST(N'2017-01-03T12:44:55.393' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (221, 4, CAST(125 AS Decimal(18, 0)), CAST(1.15 AS Decimal(18, 2)), 4, 7, CAST(N'2017-01-02T12:44:55.393' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (222, 3, CAST(469 AS Decimal(18, 0)), CAST(1.15 AS Decimal(18, 2)), 4, 7, CAST(N'2017-01-01T12:44:55.393' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (223, 2, CAST(1730 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 5, 1, CAST(N'2017-01-21T12:44:55.393' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (224, 2, CAST(519 AS Decimal(18, 0)), CAST(1.23 AS Decimal(18, 2)), 5, 2, CAST(N'2017-01-20T12:44:55.393' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (225, 2, CAST(326 AS Decimal(18, 0)), CAST(1.94 AS Decimal(18, 2)), 5, 3, CAST(N'2017-01-18T12:44:55.397' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (226, 2, CAST(727 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 5, 1, CAST(N'2017-01-19T12:44:55.397' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (227, 2, CAST(6550 AS Decimal(18, 0)), CAST(1.17 AS Decimal(18, 2)), 5, 2, CAST(N'2017-01-17T12:44:55.397' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (228, 2, CAST(435 AS Decimal(18, 0)), CAST(1.16 AS Decimal(18, 2)), 5, 3, CAST(N'2017-01-16T12:44:55.397' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (229, 2, CAST(337 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 5, 1, CAST(N'2017-01-15T12:44:55.397' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (230, 2, CAST(125 AS Decimal(18, 0)), CAST(1.15 AS Decimal(18, 2)), 5, 2, CAST(N'2017-01-14T12:44:55.397' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (231, 2, CAST(469 AS Decimal(18, 0)), CAST(1.85 AS Decimal(18, 2)), 5, 3, CAST(N'2017-01-13T12:44:55.397' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (232, 2, CAST(350 AS Decimal(18, 0)), CAST(1.16 AS Decimal(18, 2)), 5, 3, CAST(N'2017-01-12T12:44:55.397' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (233, 2, CAST(1000 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 5, 1, CAST(N'2017-01-11T12:44:55.397' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (234, 2, CAST(5689 AS Decimal(18, 0)), CAST(1.15 AS Decimal(18, 2)), 5, 2, CAST(N'2017-01-10T12:44:55.400' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (235, 3, CAST(23456 AS Decimal(18, 0)), CAST(1.85 AS Decimal(18, 2)), 5, 3, CAST(N'2017-01-09T12:44:55.400' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (236, 2, CAST(1730 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 6, 1, CAST(N'2017-01-19T12:44:55.400' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (237, 2, CAST(519 AS Decimal(18, 0)), CAST(1.23 AS Decimal(18, 2)), 6, 2, CAST(N'2017-01-18T12:44:55.400' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (238, 2, CAST(329 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 6, 1, CAST(N'2017-01-17T12:44:55.400' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (239, 2, CAST(43 AS Decimal(18, 0)), CAST(1.23 AS Decimal(18, 2)), 6, 2, CAST(N'2017-01-16T12:44:55.400' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (240, 2, CAST(133 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 6, 1, CAST(N'2017-01-15T12:44:55.400' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (241, 2, CAST(323 AS Decimal(18, 0)), CAST(1.16 AS Decimal(18, 2)), 6, 2, CAST(N'2017-01-14T12:44:55.400' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (242, 2, CAST(756 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 6, 1, CAST(N'2017-01-13T12:44:55.400' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (243, 2, CAST(234 AS Decimal(18, 0)), CAST(1.15 AS Decimal(18, 2)), 6, 2, CAST(N'2017-01-12T12:44:55.400' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (244, 2, CAST(719 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 6, 1, CAST(N'2017-01-11T12:44:55.400' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (245, 2, CAST(4343 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 6, 1, CAST(N'2017-01-09T12:44:55.400' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (246, 2, CAST(545 AS Decimal(18, 0)), CAST(1.86 AS Decimal(18, 2)), 6, 2, CAST(N'2017-01-10T12:44:55.400' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (247, 2, CAST(5756 AS Decimal(18, 0)), CAST(1.15 AS Decimal(18, 2)), 6, 2, CAST(N'2017-01-11T12:44:55.400' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (248, 2, CAST(323 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 6, 1, CAST(N'2017-01-12T12:44:55.403' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (249, 2, CAST(17030 AS Decimal(18, 0)), CAST(0.01 AS Decimal(18, 2)), 7, 5, CAST(N'2017-02-01T12:44:55.403' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (250, 2, CAST(50019 AS Decimal(18, 0)), CAST(0.23 AS Decimal(18, 2)), 7, 5, CAST(N'2017-01-31T12:44:55.403' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (251, 2, CAST(30029 AS Decimal(18, 0)), CAST(0.94 AS Decimal(18, 2)), 7, 5, CAST(N'2017-01-30T12:44:55.403' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (252, 2, CAST(43000 AS Decimal(18, 0)), CAST(0.23 AS Decimal(18, 2)), 7, 5, CAST(N'2017-01-29T12:44:55.403' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (253, 2, CAST(13300 AS Decimal(18, 0)), CAST(0.17 AS Decimal(18, 2)), 7, 5, CAST(N'2017-01-28T12:44:55.403' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (254, 2, CAST(32300 AS Decimal(18, 0)), CAST(0.16 AS Decimal(18, 2)), 7, 5, CAST(N'2017-01-27T12:44:55.403' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (255, 2, CAST(43430 AS Decimal(18, 0)), CAST(0.16 AS Decimal(18, 2)), 7, 5, CAST(N'2017-01-26T12:44:55.403' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (256, 3, CAST(54500 AS Decimal(18, 0)), CAST(0.86 AS Decimal(18, 2)), 7, 5, CAST(N'2017-01-25T12:44:55.403' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (257, 4, CAST(575600 AS Decimal(18, 0)), CAST(0.15 AS Decimal(18, 2)), 7, 5, CAST(N'2017-01-25T12:44:55.403' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (258, 3, CAST(32300 AS Decimal(18, 0)), CAST(0.85 AS Decimal(18, 2)), 7, 5, CAST(N'2017-01-24T12:44:55.407' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (259, 2, CAST(1730 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 10, 1, CAST(N'2017-01-24T12:44:55.407' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (260, 2, CAST(5019 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 10, 1, CAST(N'2017-01-23T12:44:55.407' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (261, 2, CAST(3029 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 10, 1, CAST(N'2017-01-22T12:44:55.407' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (262, 2, CAST(4300 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 10, 1, CAST(N'2017-01-21T12:44:55.407' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (263, 2, CAST(1300 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 10, 1, CAST(N'2017-01-20T12:44:55.407' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (264, 2, CAST(3200 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 10, 1, CAST(N'2017-01-19T12:44:55.407' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (265, 2, CAST(4330 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 10, 1, CAST(N'2017-01-18T12:44:55.407' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (266, 2, CAST(5400 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 10, 1, CAST(N'2017-01-17T12:44:55.407' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (267, 4, CAST(55600 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 10, 1, CAST(N'2017-01-16T12:44:55.407' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (268, 3, CAST(3300 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 10, 1, CAST(N'2017-01-15T12:44:55.407' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (269, 2, CAST(173 AS Decimal(18, 0)), CAST(1.80 AS Decimal(18, 2)), 11, 1, CAST(N'2017-01-15T12:44:55.410' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (270, 2, CAST(519 AS Decimal(18, 0)), CAST(1.23 AS Decimal(18, 2)), 11, 2, CAST(N'2017-01-14T12:44:55.410' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (271, 2, CAST(326 AS Decimal(18, 0)), CAST(1.90 AS Decimal(18, 2)), 11, 3, CAST(N'2017-01-13T12:44:55.410' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (272, 2, CAST(727 AS Decimal(18, 0)), CAST(1.23 AS Decimal(18, 2)), 11, 2, CAST(N'2017-01-12T12:44:55.410' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (273, 2, CAST(655 AS Decimal(18, 0)), CAST(1.87 AS Decimal(18, 2)), 11, 3, CAST(N'2017-01-11T12:44:55.410' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (274, 2, CAST(435 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 11, 1, CAST(N'2017-01-10T12:44:55.410' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (275, 2, CAST(337 AS Decimal(18, 0)), CAST(1.56 AS Decimal(18, 2)), 11, 2, CAST(N'2017-01-09T12:44:55.410' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (276, 2, CAST(125 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 11, 3, CAST(N'2017-01-08T12:44:55.410' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (277, 2, CAST(469 AS Decimal(18, 0)), CAST(1.15 AS Decimal(18, 2)), 11, 2, CAST(N'2017-01-07T12:44:55.410' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (278, 3, CAST(134 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 11, 1, CAST(N'2017-01-07T12:44:55.410' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (279, 1, CAST(523 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 11, 1, CAST(N'2017-01-08T12:44:55.410' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (280, 3, CAST(312 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 11, 1, CAST(N'2017-01-09T12:44:55.410' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (281, 1, CAST(723 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 11, 1, CAST(N'2017-01-10T12:44:55.410' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (282, 3, CAST(678 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 11, 1, CAST(N'2017-01-11T12:44:55.410' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (283, 1, CAST(456 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 11, 1, CAST(N'2017-01-12T12:44:55.413' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (284, 3, CAST(314 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 11, 1, CAST(N'2017-01-13T12:44:55.413' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (285, 1, CAST(161 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 11, 1, CAST(N'2017-01-14T12:44:55.413' AS DateTime))
GO
INSERT [dbo].[Deposit] ([Id], [PaymentStatusID], [PaymentAmount], [ExchangeRate], [FundingTypeID], [CurrencyID], [TimeStamp]) VALUES (286, 3, CAST(434 AS Decimal(18, 0)), CAST(1.00 AS Decimal(18, 2)), 11, 1, CAST(N'2017-01-15T12:44:55.413' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Deposit] OFF
GO
SET IDENTITY_INSERT [dbo].[FundingType] ON 

GO
INSERT [dbo].[FundingType] ([Id], [FundingTypeName], [MinDeposit], [MaxDeposit], [MonthlyQuote], [QuoteActive]) VALUES (1, N'CreditCard', CAST(50 AS Decimal(18, 0)), CAST(500 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(92690 AS Decimal(18, 0)))
GO
INSERT [dbo].[FundingType] ([Id], [FundingTypeName], [MinDeposit], [MaxDeposit], [MonthlyQuote], [QuoteActive]) VALUES (3, N'PayPal', CAST(25 AS Decimal(18, 0)), CAST(1250 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(69080 AS Decimal(18, 0)))
GO
INSERT [dbo].[FundingType] ([Id], [FundingTypeName], [MinDeposit], [MaxDeposit], [MonthlyQuote], [QuoteActive]) VALUES (4, N'UnionPay', CAST(100 AS Decimal(18, 0)), CAST(750 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(32845 AS Decimal(18, 0)))
GO
INSERT [dbo].[FundingType] ([Id], [FundingTypeName], [MinDeposit], [MaxDeposit], [MonthlyQuote], [QuoteActive]) VALUES (5, N'Skrill', CAST(75 AS Decimal(18, 0)), CAST(2500 AS Decimal(18, 0)), CAST(15000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)))
GO
INSERT [dbo].[FundingType] ([Id], [FundingTypeName], [MinDeposit], [MaxDeposit], [MonthlyQuote], [QuoteActive]) VALUES (6, N'Neteller', CAST(100 AS Decimal(18, 0)), CAST(1000 AS Decimal(18, 0)), CAST(50000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)))
GO
INSERT [dbo].[FundingType] ([Id], [FundingTypeName], [MinDeposit], [MaxDeposit], [MonthlyQuote], [QuoteActive]) VALUES (7, N'Yandex', CAST(200 AS Decimal(18, 0)), CAST(500 AS Decimal(18, 0)), CAST(25000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)))
GO
INSERT [dbo].[FundingType] ([Id], [FundingTypeName], [MinDeposit], [MaxDeposit], [MonthlyQuote], [QuoteActive]) VALUES (10, N'WebMoney', CAST(50 AS Decimal(18, 0)), CAST(1000 AS Decimal(18, 0)), CAST(35000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)))
GO
INSERT [dbo].[FundingType] ([Id], [FundingTypeName], [MinDeposit], [MaxDeposit], [MonthlyQuote], [QuoteActive]) VALUES (11, N'WireTransfer', CAST(1000 AS Decimal(18, 0)), CAST(10000 AS Decimal(18, 0)), CAST(35000 AS Decimal(18, 0)), CAST(36593 AS Decimal(18, 0)))
GO
SET IDENTITY_INSERT [dbo].[FundingType] OFF
GO
SET IDENTITY_INSERT [dbo].[FundingTypeCurrency] ON 

GO
INSERT [dbo].[FundingTypeCurrency] ([Id], [CurrencyID], [FundingTypeID]) VALUES (1, 1, 1)
GO
INSERT [dbo].[FundingTypeCurrency] ([Id], [CurrencyID], [FundingTypeID]) VALUES (2, 2, 1)
GO
INSERT [dbo].[FundingTypeCurrency] ([Id], [CurrencyID], [FundingTypeID]) VALUES (3, 3, 1)
GO
INSERT [dbo].[FundingTypeCurrency] ([Id], [CurrencyID], [FundingTypeID]) VALUES (4, 7, 4)
GO
INSERT [dbo].[FundingTypeCurrency] ([Id], [CurrencyID], [FundingTypeID]) VALUES (5, 1, 5)
GO
INSERT [dbo].[FundingTypeCurrency] ([Id], [CurrencyID], [FundingTypeID]) VALUES (6, 2, 5)
GO
INSERT [dbo].[FundingTypeCurrency] ([Id], [CurrencyID], [FundingTypeID]) VALUES (7, 3, 5)
GO
INSERT [dbo].[FundingTypeCurrency] ([Id], [CurrencyID], [FundingTypeID]) VALUES (8, 1, 6)
GO
INSERT [dbo].[FundingTypeCurrency] ([Id], [CurrencyID], [FundingTypeID]) VALUES (9, 2, 6)
GO
INSERT [dbo].[FundingTypeCurrency] ([Id], [CurrencyID], [FundingTypeID]) VALUES (10, 5, 7)
GO
INSERT [dbo].[FundingTypeCurrency] ([Id], [CurrencyID], [FundingTypeID]) VALUES (11, 1, 10)
GO
INSERT [dbo].[FundingTypeCurrency] ([Id], [CurrencyID], [FundingTypeID]) VALUES (12, 1, 11)
GO
INSERT [dbo].[FundingTypeCurrency] ([Id], [CurrencyID], [FundingTypeID]) VALUES (13, 2, 11)
GO
INSERT [dbo].[FundingTypeCurrency] ([Id], [CurrencyID], [FundingTypeID]) VALUES (14, 3, 11)
GO
INSERT [dbo].[FundingTypeCurrency] ([Id], [CurrencyID], [FundingTypeID]) VALUES (15, 5, 11)
GO
INSERT [dbo].[FundingTypeCurrency] ([Id], [CurrencyID], [FundingTypeID]) VALUES (16, 8, 11)
GO
SET IDENTITY_INSERT [dbo].[FundingTypeCurrency] OFF
GO
SET IDENTITY_INSERT [dbo].[PaymentStatus] ON 

GO
INSERT [dbo].[PaymentStatus] ([Id], [Status]) VALUES (1, N'New')
GO
INSERT [dbo].[PaymentStatus] ([Id], [Status]) VALUES (2, N'Approved')
GO
INSERT [dbo].[PaymentStatus] ([Id], [Status]) VALUES (3, N'Decline')
GO
INSERT [dbo].[PaymentStatus] ([Id], [Status]) VALUES (4, N'Technical')
GO
SET IDENTITY_INSERT [dbo].[PaymentStatus] OFF
GO
ALTER TABLE [dbo].[Deposit]  WITH NOCHECK ADD  CONSTRAINT [FK_Deposit_Currency] FOREIGN KEY([CurrencyID])
REFERENCES [dbo].[Currency] ([Id])
GO
ALTER TABLE [dbo].[Deposit] CHECK CONSTRAINT [FK_Deposit_Currency]
GO
ALTER TABLE [dbo].[Deposit]  WITH NOCHECK ADD  CONSTRAINT [FK_Deposit_FundingType] FOREIGN KEY([FundingTypeID])
REFERENCES [dbo].[FundingType] ([Id])
GO
ALTER TABLE [dbo].[Deposit] CHECK CONSTRAINT [FK_Deposit_FundingType]
GO
ALTER TABLE [dbo].[Deposit]  WITH NOCHECK ADD  CONSTRAINT [FK_Deposit_PaymentStatus] FOREIGN KEY([PaymentStatusID])
REFERENCES [dbo].[PaymentStatus] ([Id])
GO
ALTER TABLE [dbo].[Deposit] CHECK CONSTRAINT [FK_Deposit_PaymentStatus]
GO
ALTER TABLE [dbo].[FundingTypeCurrency]  WITH NOCHECK ADD  CONSTRAINT [FK_FundingTypeCurrency_Currency] FOREIGN KEY([CurrencyID])
REFERENCES [dbo].[Currency] ([Id])
GO
ALTER TABLE [dbo].[FundingTypeCurrency] CHECK CONSTRAINT [FK_FundingTypeCurrency_Currency]
GO
ALTER TABLE [dbo].[FundingTypeCurrency]  WITH NOCHECK ADD  CONSTRAINT [FK_FundingTypeCurrency_FundingType] FOREIGN KEY([FundingTypeID])
REFERENCES [dbo].[FundingType] ([Id])
GO
ALTER TABLE [dbo].[FundingTypeCurrency] CHECK CONSTRAINT [FK_FundingTypeCurrency_FundingType]
GO
/****** Object:  StoredProcedure [dbo].[GetAvailDepositIdAndFundingTypeIdForTest]    Script Date: 6/13/2017 7:09:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetAvailDepositIdAndFundingTypeIdForTest]
AS
Select d.Id DepositID, d.FundingTypeID
from Deposit d with (nolock)
	left join PaymentStatus ps with (nolock) on d.PaymentStatusID = ps.Id
	left join FundingType ft with (nolock) on d.FundingTypeID = ft.Id
where 
	d.TimeStamp between convert(datetime, '01/01/2017', 103) and convert(datetime, '01/03/2017', 103)  
	and ps.Status not like 'Approved'
group by d.Id, d.FundingTypeID, ft.MonthlyQuote
HAVING SUM(d.PaymentAmount * d.ExchangeRate) <= ft.MonthlyQuote
RETURN 0

GO
/****** Object:  StoredProcedure [dbo].[GetFundingTypeSupportedCurrencies]    Script Date: 6/13/2017 7:09:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFundingTypeSupportedCurrencies]
	@FundingTypeID int = NULL
AS

	Select 
		CASE WHEN ft.FundingTypeName IS NULL THEN '-1' ELSE ft.FundingTypeName END as FundingType
		, CASE WHEN ft.MinDeposit IS NULL THEN -1 ELSE ft.MinDeposit END as MinDeposit
		, CASE WHEN ft.MaxDeposit IS NULL THEN -1 ELSE ft.MaxDeposit END as MaxDeposit
		, c.Name as Currency
	from dbo.Currency c WITH (NOLOCK)
		left join dbo.FundingTypeCurrency ftc on c.Id = ftc.CurrencyID
		left join dbo.FundingType ft on ft.Id = ftc.FundingTypeID
	WHERE @FundingTypeID IS NULL OR ftc.FundingTypeID = @FundingTypeID;

RETURN 0

GO
/****** Object:  StoredProcedure [dbo].[SetFundingTypeDeposits]    Script Date: 6/13/2017 7:09:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SetFundingTypeDeposits]
	@FundingTypeID int,
	@DepositID int
AS
Declare @CurrentQuote int;

set @CurrentQuote = (Select 
						SUM(d.PaymentAmount * d.ExchangeRate) CurrentQuote
					from Deposit d with (nolock)
						left join PaymentStatus ps with (nolock) on d.PaymentStatusID = ps.Id
					where 
						d.TimeStamp between convert(datetime, '01/01/2017', 103) and convert(datetime, '01/03/2017', 103)  
						and ps.Status like 'Approved' -- but more  
						and d.Id = @DepositID
						and d.FundingTypeID = @FundingTypeID
					group by d.Id, d.FundingTypeID)

BEGIN TRANSACTION;

BEGIN TRY
	IF (
		@CurrentQuote is not NULL
		AND 
		@CurrentQuote <= (Select 
						ft.MonthlyQuote 
						From FundingType ft with (nolock) 
						where ft.Id = @FundingTypeID)
		AND 
		0 < (SELECT COUNT(ID) 
			FROM Deposit 
			where Id = @DepositID 
				and FundingTypeID = @FundingTypeID 
				and PaymentStatusID = (Select ps.Id from PaymentStatus ps where ps.Status not like 'Approved')
			)
		)
		OR 
		(	@CurrentQuote is null 
			AND 
			0 < (SELECT COUNT(ID) FROM Deposit where Id = @DepositID and FundingTypeID = @FundingTypeID)
		)
	BEGIN
		update Deposit 
			set PaymentStatusID = (Select ps.Id 
									from PaymentStatus ps 
									where ps.Status like 'Approved') 
		where Id = @DepositID and FundingTypeID = @FundingTypeID;
	
		update FundingType 
			set QuoteActive = 0 
		where Id = @FundingTypeID;
		--------------

		Select d.Id DepositID, d.FundingTypeID from Deposit d
		where Id = @DepositID and FundingTypeID = @FundingTypeID;

	END
END TRY
BEGIN CATCH
    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;

RETURN 0

GO
USE [master]
GO
ALTER DATABASE [Billing] SET  READ_WRITE 
GO
