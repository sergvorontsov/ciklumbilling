﻿using System;
using Microsoft.Owin.Hosting;

namespace BillingServiceWebApi
{
    class Program
    {
        static void Main()
        {
            string baseAddress = "http://localhost:8801/";

            // Start OWIN host 
            using (WebApp.Start<Startup>(url: baseAddress))
            {

                Console.WriteLine("\r\nThe web API is ready at {0}", baseAddress);
                Console.WriteLine(@"Press <Enter> to stop the owin.");
                Console.ReadLine();
            }
        }
    }
}
