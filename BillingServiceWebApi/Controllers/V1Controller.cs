﻿using System.Linq;
using System.Web.Http;
using BillingServiceWebApi.BillingServiceRef;
using BillingCommon.DTO;

namespace BillingServiceWebApi.Controllers
{
    [AllowAnonymous]
    public class V1Controller : ApiController
    {
        private readonly BillingServiceClient _client;

        public V1Controller()
        {
            _client = new BillingServiceClient("BasicHttpBinding_IBillingService");
        }

        // GET api/v1/11
        [HttpGet]
        public IQueryable<FundingTypeSupportedCurrenciesDto> GetFundingTypeSupportedCurrencyById(int id)
        {
            return _client.GetFundingTypeSupportedCurrencyById(id).AsQueryable();
        }

        // POST api/v1
        [HttpPost]
        public IQueryable<FundingTypeDto> GetAllSupportedFundingTypes()
        {
            return _client.GetAllSupportedFundingTypes().AsQueryable();
        }
        // PUT api/v1
        [HttpPut]
        public IQueryable<AvailForSetTestDto> SetFundingTypeDeposits([FromBody]AvailForSetTestDto value)
        {
            return value != null ? _client.SetFundingTypeDeposits(value.FundingTypeId, value.DepositId).AsQueryable() : null;
        }
    }
}
