﻿using NUnit.Framework;
using System.Diagnostics;
using Shouldly;
using TestStack.BDDfy;
using WcfServerConsoleTests.BillingServiceRef;

namespace WcfServerConsoleTests
{
    /*
      Results of testig:
      ---------------
      Scenario: Unit test sequences
	   Given I can all supported funding types
	   When I have avail test values for set deposit
	   Then I can set funding type deposits
    	 And I can get funding type supported currencies
     */
    public class UnitTestSequences
    {
        private readonly BillingServiceClient _client = new BillingServiceClient("BasicHttpBinding_IBillingService");
        private int _iDepositIdTest, _iFundingTypeIdTest;

        [Test]
        public void Should_Be_Complete()
        {
            this.BDDfy();
        }

        void GivenICanAllSupportedFundingTypes()
        {
            FundingTypeDto[] results = _client.GetAllSupportedFundingTypes();

            results.ShouldNotBeEmpty();
            foreach (var row in results)
                row.ShouldBeOfType<FundingTypeDto>();
        }

        void WhenIHaveAvailTestValuesForSetDeposit()
        {
            AvailForSetTestDto[] results = _client.GetAvailDepositIdAndFundingTypeIdForTest();

            results.ShouldNotBeEmpty("Not avail suitable pairs for test. Sorry...");
            foreach (AvailForSetTestDto avail in results)
            {
                _iDepositIdTest = avail.DepositId;
                _iFundingTypeIdTest = avail.FundingTypeId;
                Debug.WriteLine($"We are tested with DepositId: {_iDepositIdTest} and FundingTypeId: {_iFundingTypeIdTest}");
                break;
            }
        }

        void ThenICanSetFundingTypeDeposits()
        {
            AvailForSetTestDto[] results = _client.SetFundingTypeDeposits(_iFundingTypeIdTest, _iDepositIdTest);
            results.ShouldNotBeEmpty("Somthig wrong. No rows to update. Check error.");

            results = _client.GetAvailDepositIdAndFundingTypeIdForTest();

            results.ShouldNotBeEmpty("No available suitable pairs for test. That's all.");

            foreach (AvailForSetTestDto avail in results)
                ((avail.DepositId == _iDepositIdTest) && ( avail.FundingTypeId == _iFundingTypeIdTest))
                    .ShouldBeFalse($"Something wrong. This values (DepositId: {_iDepositIdTest} and FundingTypeId: {_iFundingTypeIdTest}) must be updated");
        }
        void AndICanGetFundingTypeSupportedCurrencies()
        {
            FundingTypeSupportedCurrenciesDto[] results = _client.GetFundingTypeSupportedCurrencies();

            results.ShouldNotBeEmpty();
            foreach (var row in results)
                row.ShouldBeOfType<FundingTypeSupportedCurrenciesDto>();
        }

    }
}
