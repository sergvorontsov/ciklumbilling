﻿namespace BillingCommon.DTO
{
    public class AvailForSetTestDto
    {
        public int DepositId { get; set; }
        public int FundingTypeId { get; set; }
    }
}
