﻿namespace BillingCommon.DTO
{
    public class FundingTypeSupportedCurrenciesDto
    {
        public string FundingType { get; set; }
        public decimal MinDeposit { get; set; }
        public decimal MaxDeposit { get; set; }
        public string Currency { get; set; }
    }
}
