﻿namespace BillingCommon.DTO
{
    public class FundingTypeDto
    {
        public int Id { get; set; }
        public string FundingType { get; set; }
        public decimal MinDeposit { get; set; }
        public decimal MaxDeposit { get; set; }
        public decimal MonthlyQuote { get; set; }
    }
}
