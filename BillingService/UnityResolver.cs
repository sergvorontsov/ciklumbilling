﻿using System.Configuration;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace BillingService
{
    public class UnityResolver
    {
        private static IUnityContainer _container;

        public static IUnityContainer Container
        {
            get { return _container; }
            private set { _container = value; }
        }

        static UnityResolver()
        {
            var container = new UnityContainer();

            var section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            if (section != null)
                section.Configure(container);

            _container = container;
        }

        public static T Resolve<T>()
        {
            T ret = default(T);

            if (Container.IsRegistered(typeof(T)))
            {
                ret = Container.Resolve<T>();
            }

            return ret;
        }
    }
}