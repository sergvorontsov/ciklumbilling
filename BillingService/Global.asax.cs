﻿using System;
using BI;
using Microsoft.Practices.Unity;

namespace BillingService
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            UnityResolver.Container.RegisterType(typeof(IFundingTypeRepository), typeof(FundingTypeRepository), new HierarchicalLifetimeManager());
        }
   }
}