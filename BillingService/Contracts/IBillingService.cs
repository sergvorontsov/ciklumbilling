﻿using System.Linq;
using System.ServiceModel;
using BillingCommon.DTO;

namespace BillingService.Contracts
{
    [ServiceContract]
    public interface IBillingService
    {
        [OperationContract]
        IQueryable<FundingTypeDto> GetAllSupportedFundingTypes();

        [OperationContract]
        IQueryable<FundingTypeDto> GetAllSupportedFundingTypeById(int iFundingTypeId);

        [OperationContract]
        IQueryable<FundingTypeSupportedCurrenciesDto> GetFundingTypeSupportedCurrencies();

        [OperationContract]
        IQueryable<FundingTypeSupportedCurrenciesDto> GetFundingTypeSupportedCurrencyById(int iFundingTypeId);

        [OperationContract]
        IQueryable<AvailForSetTestDto> SetFundingTypeDeposits(int iFundingTypeId, int iDepositId);

        [OperationContract]
        IQueryable<AvailForSetTestDto> GetAvailDepositIdAndFundingTypeIdForTest();
    }
}
