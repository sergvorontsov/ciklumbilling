﻿using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using BillingCommon.DTO;
using BI;
using log4net;

namespace BillingService.Contracts
{
    public class BillingService : IBillingService
    {
        private readonly IFundingTypeRepository _repository;
        private static readonly ILog _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public BillingService() : this(UnityResolver.Resolve<IFundingTypeRepository>())
        {
            _logger.Debug($"called ctor::BillingService()");
        }
        public BillingService(IFundingTypeRepository repository)
        {
            _repository = repository;
        }

        public IQueryable<FundingTypeDto> GetAllSupportedFundingTypes()
        {
            return _repository.GetAllSupportedFundingTypes();
        }

        public IQueryable<FundingTypeDto> GetAllSupportedFundingTypeById(int iFundingTypeId)
        {
            return _repository.GetAllSupportedFundingTypeById(iFundingTypeId);
        }

        public IQueryable<FundingTypeSupportedCurrenciesDto> GetFundingTypeSupportedCurrencies()
        {
            return _repository.GetFundingTypeSupportedCurrencies();
        }

        public IQueryable<FundingTypeSupportedCurrenciesDto> GetFundingTypeSupportedCurrencyById(int iFundingTypeId)
        {
            return _repository.GetFundingTypeSupportedCurrencyById(iFundingTypeId);
        }

        public IQueryable<AvailForSetTestDto> SetFundingTypeDeposits(int iFundingTypeId, int iDepositId)
        {
            return _repository.SetFundingTypeDeposits(iFundingTypeId, iDepositId);
        }

        public IQueryable<AvailForSetTestDto> GetAvailDepositIdAndFundingTypeIdForTest()
        {
            return _repository.GetAvailDepositIdAndFundingTypeIdForTest();
        }
    }
}
